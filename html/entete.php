<!-- En tête de la page -->
<header id="header-desktop">
    <div class="wrapper" id="entete">

        <h1>Robot Index<span class="green">.</span></h1>

        <nav>
            <ul>
                <li><a href="/">Robot</a></li>
                <li><a href="/recent.php">Sites</a></li>
                <li><a href="/top.php">Top 50</a></li>                
            </ul>
        </nav>

    </div>
</header>


<header id="header-smartphone">
    <div id="entete">

          <!-- Top Navigation Menu Smartphone -->
            <div class="topnav">
            <h1 id="nav-title">Robot Index<span class="green">.</span></h1>                
                <div id="myLinks">
                    <ul>
                        <li><a href="/">Robot</a></li>
                        <li><a href="/recent.php">Sites</a></li>
                        <li><a href="/top.php">Top 50</a></li>                        
                    </ul>
                </div>
                <a href="javascript:void(0);" class="ham-icon" onclick="myFunction()"> 
                    <img id="ham-ico" src="https://art-dambrine.ovh/learning/css/hamburger.png">
                    <img id="cross-ico" src="https://art-dambrine.ovh/learning/css/cross.png">
                </a>
            </div>

    </div>
</header>