<?php
if (isset($_GET['port'])){

$port = $_GET['port'];
$port = (int) $port;

if ($port>8090) die();

$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => "http://art-dambrine.ovh:".$port,
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "GET",
  CURLOPT_HTTPHEADER => array(
    "Content-Type: application/json"
  ),
));

$response = curl_exec($curl);

curl_close($curl);

echo $response;

}