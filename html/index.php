<?php  /* Projet learning 2019 */
include("head.php");
include("entete.php");
?>

<!-- Corps de la page -->
<div class="content">
        <div class="content-inside">
                <h2>Robot Index</h2>

                <p>
                        Notre robot teste et indexe les liens du site de votre choix <br> <br>
                        Commencez par copier l'adresse d'un site depuis la bare d'adresse de votre navigateur.
                </p>

                <br/>

                <form action="robot.php" method="POST" class="form-example">
                    <div class="form-example">
                        <label for="name"><strong>URL</strong></label>                        
                        <input type="url" name="URL" id="URL" placeholder="Collez ici l'URL" required>
                    </div>
                    <div class="form-example">
                        <label for="number">Nombre de pages à parcourir </label>
                        <input type="number" name="number" id="number" value="30" min ="1" max="50" required>
                    </div>
                    <div class="form-example">
                        <input type="submit" value="Lancez le Robot !">
                    </div>
                </form>

                <br>

                <h3><a style="color: #0C62A6;" href="top.php">Voir le Top 50 des liens indexés</a></h3>
                <h3><a style="color: #0C62A6;" href="recent.php">Voir les resultats des sites testés recement</a></h3>




                
                
        </div>
</div>

<?php include("pieddepage.php");
