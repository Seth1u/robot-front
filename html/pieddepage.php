<!-- Pied de la page -->
<footer id="footer">

        <a href="#"><img class="gotopbtn" src="https://art-dambrine.ovh/learning/css/up-arrow.png" title="up-arrow" alt="up-arrow"></a>
        <div class="container">
            <div class="copyright">Copyright © 2019<span class="green">.</span> Arthur Dambrine & Seth Bio Yara<span class="green">.</span></div>
        </div>
</footer>

<script>
function myFunction() {
  var x = document.getElementById("myLinks");
  if (x.style.display === "block") {
    x.style.display = "none";
  } else {
    x.style.display = "block";
  }

  var y = document.getElementById("header-smartphone");
  var title = document.getElementById("nav-title");
  if (x.style.display === "block") {
     y.style.margin = "0px 0px 165px 0px"; /* Aggrandissement du header à l'apparition du menu */
     title.style.padding = "14px 16px 0px 16px";
  } else {
    y.style.margin = "0px 0px 0px 0px";
    title.style.padding = "14px 16px";
  }

  var cross = document.getElementById("cross-ico");
  var ham = document.getElementById("ham-ico");
  if (x.style.display === "block") {
    cross.style.display = "block";
    ham.style.display = "none";
  } else {
    cross.style.display = "none";
    ham.style.display = "block";
  }

}
</script>