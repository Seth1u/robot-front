<?php  /* Projet learning 2019 */

ini_set('display_errors', 1);
error_reporting(~0);


include("head.php");
include("entete.php");
include("bdd.php");

?>

<!-- Corps de la page -->
<div class="content">
        <div class="content-inside">
                <h1>Sites testés il y a peu</h1>
                <h3>par <strong>Robot Index</strong></h3>
                <p> <strong>Cliquez sur les liens</strong> pour afficher le resultat des tests<p>                                

                <table class="tab-result">
                <tbody>
                    <?php 
                        //phpinfo();                                                                                                
                        $requete = $bdd -> prepare('
                        SELECT url, creation_date as "most_recent" 
                        FROM robot.SITE 
                        WHERE length(url) > 2 AND url NOT like "%cdn%" AND url like "%.%" AND url NOT like "%:%:%"                        
                        ORDER BY creation_date DESC
                        LIMIT 20;
                        ');
                        $requete->execute();                      

                        while ($donnes = $requete->fetch()){                            
                            echo '<tr><th><a style="color: #0C62A6;" id="sites-recents" href="site_info.php?url='.$donnes['url'].' ">' . parse_url($donnes['url'], PHP_URL_HOST). '</a></th></tr>';                                                        
                        }
// site_info.php?url=http://www.arngren.net                        
                    ?>
                </tbody>
                </table>


                
                
        </div>
</div>

<?php include("pieddepage.php");

