<?php

/* Defining a PHP Function */
function availablePort($min_port, $max_port) {

    for($port = $min_port; $port <= $max_port; $port++){
        // create curl resource 
        $ch = curl_init(); 
        // set url  ---> /!\ IMPORTANT Laisser en http (nécessaire)
        curl_setopt($ch, CURLOPT_URL, "http://art-dambrine.ovh:".$port);
        //return the transfer as a string 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
        // $output contains the output string 
        $output = curl_exec($ch);     
        // close curl resource to free up system resources 
        curl_close($ch);   
    
        //echo $output;
    
        if($output == null){
            //echo $port.' is unused port </br>';
            return $port;
        } else {
            echo 'Robot is busy.</br>';
            if($port == $max_port)
                return false;
        }
    }       
 }

 //function isWorkingSite($url) 
 function isSiteAvailible($url){
    // Check, if a valid url is provided
    if(!filter_var($url, FILTER_VALIDATE_URL)){
        return false;
    }

    // Initialize cURL
    $curlInit = curl_init($url);
    
    // Set options
    curl_setopt($curlInit,CURLOPT_CONNECTTIMEOUT,10);
    curl_setopt($curlInit,CURLOPT_HEADER,true);
    curl_setopt($curlInit,CURLOPT_NOBODY,true);
    curl_setopt($curlInit,CURLOPT_RETURNTRANSFER,true);

    // Get response
    $response = curl_exec($curlInit);
    
    // Close a cURL session
    curl_close($curlInit);

    return $response?true:false;
}


/* Projet learning 2019 */
include("head.php");
include("entete.php");

$regex_url = "%^(?:(?:https?|ftp)://)(?:\S+(?::\S*)?@|\d{1,3}(?:\.\d{1,3}){3}|(?:(?:[a-z\d\x{00a1}-\x{ffff}]+-?)*[a-z\d\x{00a1}-\x{ffff}]+)(?:\.(?:[a-z\d\x{00a1}-\x{ffff}]+-?)*[a-z\d\x{00a1}-\x{ffff}]+)*(?:\.[a-z\x{00a1}-\x{ffff}]{2,6}))(?::\d+)?(?:[^\s]*)?$%iu" ;
$url = $_POST['URL'];

$urlIsVerified = false;
$clean_url = $url;

if (preg_match($regex_url, $url))
{
    $scheme = parse_url($url, PHP_URL_SCHEME);
    $domain = parse_url($url, PHP_URL_HOST);

    $clean_url = $scheme."://".$domain;
    
    $urlIsVerified = true;
}
?>

<!-- Corps de la page -->
<div class="content">
        <div class="content-inside">
                <h2>Robot Index</h2> <br>

                <?php 

                $iterations = $_POST['number'];
                if ($iterations >= 50){
                    $iterations = 50;
                }
                echo'<div id="site"><p>';
                if ($launchJava = isSiteAvailible($_POST['URL'])){
                    echo $clean_url;
                } else {
                    echo '<p>Désolé ' . $url .' n\'est pas une url valide pour notre robot <br>';
                    echo 'Pouvez vous utiliser une url copiée directement 
                    <br> depuis la barre d\'adresse du navigateur ? :) </p>';
                }                
                echo'</p></div>';

                echo '<p>';
                echo '</br>';
                echo 'Pages à visiter : '. $iterations;
                echo '</br>';                

                // Tester si $_POST['URL'] est une URL valide

                // Checkin what port to use for the JAVA Robot WebServer
                $available_port = availablePort(8085, 8088);
                
                echo 'Notification port : <strong id="port">' . $available_port .'</strong><br>';                
                            
                $java_shell_exec = "cd /var/www/java && 
                nohup java -cp mysql-connector-java.jar:RobotIndex.jar robotindex.RobotIndex " .$clean_url. " " . $iterations . " " .$available_port." &";
                    
                    // code JAVA exec
                    if($launchJava && $urlIsVerified && $available_port!=false){
                        //echo 'Launch JAVA cmd :' . $java_shell_exec;
                        echo '</br>';
                        // to execute a command in background <3
                        pclose(popen($java_shell_exec, 'r'));                     
                    } else {
                        echo 'Essayer une url differente.';
                    }
                
                echo '</p>'; 
                
                
                ?>

                <br/>
                <h4>Dialog-box: </h4>
                <p id="dialogbox"></p>
                <div class="text-center" style="margin-left: auto; margin-right:auto;">
                    <progress id="progressbar" value="0" max="100"></progress>
                </div>
                
                
        </div>
</div>

<script>

// Boîte de dialogue pour afficher l'avancement 
function loopNotif()
{
//Continuez le traitement après la pause dans cette fonction
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");

    var requestOptions = {
    method: 'GET',
    headers: myHeaders,
    redirect: 'follow'
    };

    
    fetch("../get_avancement.php?port=" + document.getElementById("port").textContent, requestOptions)
    .then(function(response) {
        return response.text();
    })
    .then(function(myText) {
    //console.log("TEST: result :" + myText);
    var dialogbox = document.getElementById("dialogbox");

    previous = dialogbox.textContent;
    console.log("previous: " + previous);

    if(myText==""){        

        myText = "rien a afficher";

        if(previous!="" && previous != "rien a afficher"){
            console.log("changer de page!!!");
            // Simulate an HTTP redirect:
            var url = document.getElementById("site");            
            window.location.replace("site_info.php?url=" + url.textContent);
        }
    }

     // Parse JSON here and get progression :   
    //myText

    const json = myText;
    const obj = JSON.parse(json);
    // obj.progression; pourcentage de progression
    document.getElementById("progressbar").value = obj.progression;    
    dialogbox.textContent = obj.message;

    
    
    })
    .then(result => console.log(result))    
    .catch(error => console.log('error', error));
    }
    
    setInterval(loopNotif, 1000);


</script>

<?php include("pieddepage.php");

