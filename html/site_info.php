<?php  /* Projet learning 2019 */

ini_set('display_errors', 1);
error_reporting(~0);

if (isset($_GET['url'])){
include("head.php");
include("entete.php");
include("bdd.php");
?>

<!-- Corps de la page -->
<div class="content">
        <div class="content-inside">
                <h2>Robot Index</h2>

                <?php echo '<h4>' . $_GET['url'] . '</h4></br>';?>

                <table class="tab-result">
                <tbody>
                    <?php 
                        //phpinfo();                                                                                                
                        $requete = $bdd -> prepare('
                        SELECT url, creation_date, link, score
                        FROM SITE, LINK 
                        WHERE url = site_id 
                            AND url = ? AND length(link) > 2 AND link NOT like "%cdn%" AND link like "%.%"
                        ORDER BY score DESC;
                        ');
                        $requete->execute(array($_GET['url']));  
                        // Declaration du tableau associatif                    
            
                        while ($donnes = $requete->fetch()){                            
                            echo '<tr><th>' . $donnes['link'] . '</th>';                            
                            echo '<td id="td-padding">' . $donnes['score'] . '</td></tr>';
                        }
                        
                    ?>
                </tbody>
                </table>


                
                
        </div>
</div>

<?php include("pieddepage.php");

} else {
    echo 'Manque un parametre url';
} // fin du isset
