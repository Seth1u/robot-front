<?php  /* Projet learning 2019 */

ini_set('display_errors', 1);
error_reporting(~0);


include("head.php");
include("entete.php");
include("bdd.php");

// SELECT count(*) FROM robot.SITE WHERE url NOT like "%:%:%";
$requete = $bdd -> prepare('
                SELECT count(*) 
                FROM robot.SITE 
                WHERE url NOT like "%:%:%";');

$requete->execute(); 
$nbsites = $requete->fetch();
?>

<!-- Corps de la page -->
<div class="content">
        <div class="content-inside">

            <div class="text-padding">

            <h1>Top 50 des liens</h1>
                <h3>trouvés par <strong>Robot Index</strong></h3>
                <p> <strong>Liens externes apparus</strong> le plus souvent parmi les <strong> <?php echo $nbsites['count(*)'] ?> sites </strong> testés avec notre robot<p>
                <p> <strong>O : occurences</strong> somme des apparitions de ce lien - <strong>P : pertinence</strong> nombre de sites visant ce lien.<p>          

            </div>
                                          

                <table class="tab-result">
                <tbody>
                    <?php 
                        //phpinfo();                                                                                                
                        $requete = $bdd -> prepare('
                        SELECT link, sum(score) as "sum" , count(link) as "from_nb_sites"
                        FROM LINK 
                        WHERE length(link) > 2 AND link NOT like "%cdn%" AND link like "%.%"
                        GROUP BY link
                        HAVING count(link) > 1	
                        ORDER BY count(link) DESC
                        LIMIT 50;
                        ');
                        $requete->execute();  
                                          
                        echo '<div class="tab-top-50">
                                <tr class="table-head-top-50">
                                <td>Liens</td>
                                <td id ="td-padding">O</td>
                                <td id ="td-padding">P</td>
                                </tr>';

                        while ($donnes = $requete->fetch()){                            
                            echo '<tr><th><a style="color: #0C62A6;" href="//'.$donnes['link'].'">' . $donnes['link'] . '</a></th>';
                            echo '<td id ="td-padding">' . $donnes['sum'] . '</td>';                            
                            echo '<td id ="td-padding">' . $donnes['from_nb_sites'] . '</td></tr> </div">';
                        }
                        
                    ?>
                </tbody>
                </table>


                
                
        </div>
</div>

<?php include("pieddepage.php");

